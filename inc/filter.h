#ifndef FILTER_H
#define FILTER_H

#include <iostream>
#include "image.h"

using namespace std;

class Filter {
    public:
        Filter(){};
        virtual void apply(Image *img){};
};

#endif
