#ifndef BW_H
#define BW_H

#include "filter.h"

class BW : public Filter {
    public:
        BW(){};
        void apply(Image *img); 

};

#endif
