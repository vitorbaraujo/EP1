#ifndef NEGATIVE_H
#define NEGATIVE_H

#include "filter.h"

class Negative : public Filter {
    public:
        Negative(){};
        void apply(Image *img);
};

#endif
