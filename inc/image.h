#ifndef IMAGE_H
#define IMAGE_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>

using namespace std;

class Image {
    private:
        int h, w, max_gray;
        string magic_number;
        unsigned char ***pixels;

    public:
        Image();
        void parse_image(string path);
        void set_h(int mh);
        void set_w(int mw);
        void set_max_gray(int mg);
        void set_magic_number(string mn);
        int get_h();
        int get_w();
        int get_max_gray();
        string get_magic_number();
        unsigned char*** get_pixels();
};

#endif
