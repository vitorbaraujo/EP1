#ifndef POLARIZED_H
#define POLARIZED_H

#include "filter.h"

class Polarized : public Filter {
    public:
        Polarized(){};
        void apply(Image *img);
};

#endif
