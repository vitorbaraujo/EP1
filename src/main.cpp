#include <iostream>
#include "image.h"
#include "filter.h"
#include "bw.h"
#include "negative.h"
#include "polarized.h"

using namespace std;

void menu(){
    cout << "Digite o número correspondente ao filtro desejado:" << endl;
    cout << "1 - Preto e Branco" << endl;
    cout << "2 - Negativo" << endl;
    cout << "3 - Polarizado" << endl;
    cout << "4 - Média" << endl;
}

int main(){
    Image *img = new Image();
    Filter *filter;
    string name;
    int option;

    cout << "Digite o nome da imagem (sem o .ppm)" << endl;
    cin >> name;

    img->parse_image(name);

    menu();

    while(scanf("%d", &option), option != 5){
        switch(option){
            case 1:
                filter = new BW();
                filter->apply(img);
                break;
            case 2:
                filter = new Negative();
                filter->apply(img);
                break;
            case 3:
                filter = new Polarized();
                filter->apply(img);
                break;
            case 4:
                // filter = new Median();
                // filter->apply(img);
                break;
            case 5:

                break;
            default:
                cout << "Opção inválida" << endl;
        }
    }

    return 0;
}
