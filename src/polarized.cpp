#include "polarized.h"

void Polarized::apply(Image *img){
    unsigned char ***pixels = img->get_pixels();
    int height = img->get_h();
    int width = img->get_w();
    int max_gray = img->get_max_gray();

    ofstream outfile("doc/result3.ppm"); 

    outfile << img->get_magic_number() << endl;
    outfile << height << " " << width << endl;
    outfile << max_gray << endl;

    for(int i=0;i<width;i++){
        for(int j=0;j<height;j++){
            int r, g, b;

            if((int)pixels[i][j][0] < max_gray/2)
                r = 0;
            else
                r = max_gray;

            if((int)pixels[i][j][1] < max_gray/2)
                g = 0;
            else
                g = max_gray;

            if((int)pixels[i][j][2] < max_gray/2)
                b = 0;
            else
                b = max_gray;

            outfile << (unsigned char)r << (unsigned char)g << (unsigned char)b;
        }
    }

    cout << "Filtro polarizado aplicado com sucesso!" << endl;
    outfile.close();
}
