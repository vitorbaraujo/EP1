#include "negative.h"

void Negative::apply(Image *img){
    unsigned char ***pixels = img->get_pixels();
    int height = img->get_h();
    int width = img->get_w();
    int max_gray = img->get_max_gray();

    ofstream outfile("doc/result2.ppm"); 

    outfile << img->get_magic_number() << endl;
    outfile << height << " " << width << endl;
    outfile << max_gray << endl;

    for(int i=0;i<width;i++){
        for(int j=0;j<height;j++){
            for(int k=0;k<3;k++){
                int negative_value = max_gray - (int)pixels[i][j][k];
                outfile << (unsigned char)negative_value;
            }
        }
    }

    cout << "Filtro negativo aplicado com sucesso!" << endl;
    outfile.close();
}
