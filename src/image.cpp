#include "image.h"
#define uc unsigned char

Image::Image(){

}

void Image::set_h(int mh){
    h = mh;
}

void Image::set_w(int mw){
    w = mw;
}

void Image::set_max_gray(int mg){
    max_gray = mg;
}

void Image::set_magic_number(string mn){
    magic_number = mn;
}

int Image::get_h(){
    return h;
}

int Image::get_w(){
    return w;
}

int Image::get_max_gray(){
    return max_gray;
}

string Image::get_magic_number(){
    return magic_number;
}

void Image::parse_image(string path){

    path = "doc/" + path + ".ppm";

    ifstream file(path.c_str());

    string s[5];

    if(not file.is_open()){
        cout << "Erro na abertura do arquivo" << endl;
        return;
    }
    else{
        string line;
        int counter = 0;
        for(int i=0;i<4;i++){
            if(counter == 4) break;
            getline(file, line);

            if(line[0] == '#') i--;
            else{
                stringstream b(line); 
                string temp;
                while(b >> temp){
                    if(counter == 4 || temp[0] == '#') break;

                    switch(counter){
                        case 0:
                            magic_number = temp;
                            break;
                        case 1:
                            h = atoi(temp.c_str());
                            break;
                        case 2:
                            w = atoi(temp.c_str());
                            break;
                        case 3:
                            max_gray = atoi(temp.c_str());
                            break;
                    }
                    counter++;
                }
            }
        }
    }

    pixels = (uc***)malloc(w * sizeof(uc***));
    for(int i=0;i<w;i++)
        pixels[i] = (uc**)malloc(h * sizeof(uc**));
    for(int i=0;i<w;i++){
        for(int j=0;j<h;j++){
            pixels[i][j] = (uc*)malloc(3 * sizeof(uc*));
        }
    }

    for(int i=0;i<w;i++){
        for(int j=0;j<h;j++){
            for(int k=0;k<3;k++){
                pixels[i][j][k] = file.get();
            }
        }
    }
}

unsigned char*** Image::get_pixels(){
    return pixels;
}
