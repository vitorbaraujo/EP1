#include "bw.h"

void BW::apply(Image *img){
    unsigned char*** pixels = img->get_pixels();

    int height = img->get_h();
    int width = img->get_w();

    ofstream outfile("doc/result.ppm");

    outfile << img->get_magic_number() << endl;
    outfile << img->get_h() << " " << img->get_w() << endl;
    outfile << img->get_max_gray() << endl;

    for(int i=0;i<width;i++){
        for(int j=0;j<height;j++){
            int grayscale_value = (0.299 * (int)pixels[i][j][0]) + (0.587 * (int)pixels[i][j][1]) + (0.144 * (int)pixels[i][j][2]);

            if(grayscale_value > 255) grayscale_value = 255;

            outfile << (unsigned char)grayscale_value << (unsigned char)grayscale_value << (unsigned char)grayscale_value;
        }
    }

    cout << "Filtro Preto e Branco aplicado com sucesso!" << endl;

    outfile.close();
}
